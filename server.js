require('dotenv').config()
const express = require('express')
const cors = require('cors')
const readdirp = require('readdirp')
const path = require('path')
const CronJob = require('cron').CronJob
const mysql = require('mysql2/promise')
const db = require('./models')
const todayExpenseTrigger = require('./triggers/todayExpenseTrigger')

const app = express()
app.use(express.json())

//** CORS OPTION */
const corsOptionsDelegate = function (req, callback) {
    callback(null, { origin: true })
}
app.use(cors(corsOptionsDelegate))

//** Static route for get file location */
app.use('/files', express.static('uploads'))
app.use('/files', express.static('assets'))

//** Route Loader */
function routeLoader(route, routes_type) {
    let splitRoute;
    if (process.platform == 'win32')
        splitRoute = route.path.split('\\')[1]
    else
        splitRoute = route.path.split('/')[1]
    if (splitRoute.includes(routes_type)) {
        const file = require(`./${route.path}`)
        app.use(file)
        console.log(`Route loader:\t${path.basename(route.path)}`);
    }
}

//** Express start */
const port = process.env.NODE_SERVER_PORT
app.listen(port, () => {
    console.log(`The app was listening in ${process.env.NODE_HOSTNAME}`);
})


//** Load path of the route and sorting it */
readdirp('.', { fileFilter: '*Route.js' })
    .on('data', (route) => {
        routeLoader(route, 'rootRoute.js')
    })
    .on('data', (route) => {
        routeLoader(route, 'public')
    })
    .on('data', (route) => {
        routeLoader(route, 'private')
    })

//** database connection */
db.sequelize
    .authenticate()
    .then(function (err) {
        console.log('DB Connection:\tSuccess');
    })
    .catch(async function (err) {
        if (err.original.sqlMessage == `Unknown database '${config.database}'`) {
            const connectionQuery = { host: config.host, user: config.username, password: config.password }
            const connection = await mysql.createConnection(connectionQuery);
            await connection.query(`CREATE DATABASE IF NOT EXISTS \`${config.database}\`;`);
            console.log(`Database '${config.database}' not found, create it first`)
        } else {
            console.log('DB Connection:\t', err);
        }
    });

//** Cron Job function */
var job = new CronJob('* * * * *', function () {
    todayExpenseTrigger()
}, null, true, 'Asia/Jakarta');
job.start()
