const express = require('express')

const app = express.Router()

app.get('/', (req, res) => {
    res.status(200).send('welcome to FunD web API')
})

module.exports = app