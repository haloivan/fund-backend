const express = require('express')
const UserController = require('../../../controllers/userController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()

app.post('/register', async (req, res, next) => {
    try {
        const user = new UserController(req.body)
        await user.register()
        res.status(200).send(user.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app