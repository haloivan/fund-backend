const express = require('express')
const ProductController = require('../../../controllers/productController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')


const app = express.Router()
app.get('/products/popular', async (req, res, next) => {
    try {
        const product = new ProductController(req.query)
        await product.getProductByPopular()
        res.status(200).send(product.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app

