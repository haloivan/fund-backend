const express = require('express')
const passport = require('passport')
const BankAccountController = require('../../../controllers/bankAccountController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()

app.use(passport.authenticate('bearer', { session: false }))

app.delete('/bankAccounts/:id', async (req, res, next) => {
    try {
        const bankAccount = new BankAccountController(req.params)
        await bankAccount.deleteBankAccount()
        res.status(200).send('bankAccount have been deleted')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app