const express = require('express')
const passport = require('passport')
const BankAccountController = require('../../../controllers/bankAccountController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()

app.use(passport.authenticate('bearer', { session: false }))

app.get('/bankAccounts', async (req, res, next) => {
    try {
        const bankAccount = new BankAccountController(req.query)
        await bankAccount.getBankAccount()
        res.status(200).send(bankAccount.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app