const express = require('express')
const ProductController = require('../../../controllers/productController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const app = express.Router()

app.delete('/products/:id', async (req, res) => {
    try {
        res.status(200).send('NOT USABLE RIGHT NOW')
        // const product = new ProductController(req.params)
        // await product.deleteProduct()
        // res.status(200).send('Product have been deleted')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app