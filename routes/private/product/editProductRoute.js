const express = require('express')
const ProductController = require('../../../controllers/productController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const app = express.Router()

app.patch('/products', async (req, res, next) => { 
    try {
        const product = new ProductController(req.body)
        await product.updateProduct()
        res.status(200).send('Data product have been update')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app