const express = require('express')
const BudgetController = require('../../../controllers/budgetController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()

app.delete('/budgets/:id', async (req, res, next) => {
    try {
        const budget = new BudgetController(req.params)
        await budget.deleteBudget()
        res.status(200).send('budget have been deleted')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app