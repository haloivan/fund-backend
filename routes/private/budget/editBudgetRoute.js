const express = require('express')
const BudgetController = require('../../../controllers/budgetController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const passport = require('../../../middleware/authMiddleware')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.patch('/budgets', async (req, res, next) => {
    try {
        const budget = new BudgetController(req.body)
        await budget.editBudget()
        res.status(200).send('Data budget have been update')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app