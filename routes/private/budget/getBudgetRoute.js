const express = require('express')
const BudgetController = require('../../../controllers/budgetController')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.get('/budgets', async (req, res, next) => {
    try {
        const budget = new BudgetController(req.query)
        await budget.getBudget()
        res.status(200).send(budget.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app