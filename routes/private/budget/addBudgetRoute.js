const express = require('express')
const BudgetController = require('../../../controllers/budgetController')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.post('/budgets', async (req, res, next) => {
    try {
        const budget = new BudgetController(req.body)
        await budget.addBudget()
        res.status(200).send(budget.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app