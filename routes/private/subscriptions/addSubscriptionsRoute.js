const express = require('express')
const SubscriptionsController = require('../../../controllers/subscriptionsController')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.post('/subscription', async (req, res, next) => {
    try {
        const subscription = new SubscriptionsController(req.body)
        await subscription.addSubscriptions()
        res.status(200).send(subscription.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app