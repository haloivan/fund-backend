const express = require('express')
const SubscriptionsController = require('../../../controllers/subscriptionsController')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.get('/subscription', async (req, res, next) => {
    try {
        const subscription = new SubscriptionsController(req.query)
        await subscription.userActiveSubscribe()
        res.status(200).send(subscription.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app