const express = require('express')
const UserController = require('../../../controllers/userController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const passport = require('../../../middleware/authMiddleware')

const app = express.Router()

app.use(passport.authenticate('bearer', { session: false }))

app.get('/users/:id', async (req, res, next) => {
    try {
        const user = new UserController(req.params)
        await user.getUser()
        res.status(200).send(user.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app
