const express = require('express')
const UserController = require('../../../controllers/userController')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const passport = require('../../../middleware/authMiddleware')

const app = express.Router()

app.use(passport.authenticate('bearer', { session: false }))

app.patch('/users', async (req, res, next) => {
    try {
        const user = new UserController(req.body)
        await user.updateUser()
        res.status(200).send('Data user have been update')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app
