const express = require('express')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const WalletController = require('../../../controllers/walletController')
const db = require('../../../models')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.get('/wallets', async (req, res, next) => {
    try {
        const wallet = new WalletController(req.query)
        await wallet.getWallet()
        res.status(200).send(wallet.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app