const express = require('express')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const WalletController = require('../../../controllers/walletController')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.patch('/wallets', async (req, res, next) => {
    try {
        const wallet = new WalletController(req.body)
        await wallet.topupWallet()
        res.status(200).send('Data wallet have been update')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app
