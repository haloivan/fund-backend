const express = require('express')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const WalletController = require('../../../controllers/walletController')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.delete('/wallets/:id', async (req, res, next) => {
    try {
        const wallet = new WalletController(req.params)
        await wallet.deleteWallet()
        res.status(200).send('wallet have been deleted')
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app