const express = require('express')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')
const WalletController = require('../../../controllers/walletController')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.post('/wallets', async (req, res, next) => {
    try {
        const wallet = new WalletController(req.body)
        await wallet.addWallet()
        res.status(200).send(wallet.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app