const express = require('express')
const ExpenseController = require('../../../controllers/expenseController')
const passport = require('../../../middleware/authMiddleware')
const routeErrorHandler = require('../../../middleware/errorMiddleware')

const app = express.Router()
app.use(passport.authenticate('bearer', { session: false }))

app.get('/expense/history/:userId', async (req, res, next) => {
    try {
        const expense = new ExpenseController(req.params)
        await expense.getHistory()
        res.status(200).send(expense.data)
    } catch (error) {
        next(error)
    }
})

app.use(routeErrorHandler)

module.exports = app