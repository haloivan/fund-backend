"use strict";
const nodemailer = require('nodemailer');

async function emailSender(msg, recipient) {
    const transporter = nodemailer.createTransport({
        host: process.env.NOTIFICATION_HOST,
        port: 465,
        secure: true,
        auth: {
            user: process.env.NOTIFICATION_USER,
            pass: process.env.NOTIFICATION_PASS,
        },
    });

    const mailOptions = {
        from: '"FUNd Team" <fund.notifier@gmail.com>',
        to: `${recipient}, <${recipient}>`,
        subject: "FunD App Notification",
        text: `Please do not reply this message, because it send using automatic system.
        We do not provide any problem that caused by replying this message.
        \n
        \n
        ${msg}
        \n
        \n
        Send with love,
        \n
        FunD Team | Glints Academy X Binar Academy #8`
    }

    await transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            console.log(err);
        } else {
            console.log('Message sent: ' + info);
        }
    });
}

module.exports = emailSender