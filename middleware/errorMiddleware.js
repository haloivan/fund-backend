const CustomError = require("../helpers/customErrorHelper");

errSchema = {
    ER_BAD_FIELD_ERROR: {
        status: 400,
        message: "Bad field inserted"
    },
    ER_DATA_TOO_LONG: {
        status: 400,
        message: "Data too long"
    },
    ERR_NOT_FOUND: {
        status: 404,
        message: "Data not found"
    },
    ER_DUP_ENTRY: {
        status: 409,
        message: "Duplicate entry"
    },
    ER_NO_REFERENCED_ROW_2: {
        status: 400,
        message: "Id is not related"
    },
    ER_ROW_IS_REFERENCED_2: {
        status: 400,
        message: "Bad field Id is related to another data"
    }
}


function routeErrorHandler(err, req, res, next) {
    // Token based error are handled by passport.js
    if (!['TokenExpiredError', 'JsonWebTokenError'].includes(err.name)) {
        if (Object.keys(errSchema).includes(err.code))
            res.status(errSchema[err.code].status).json({
                status: errSchema[err.code].status,
                name: errSchema[err.code].status,
                message: errSchema[err.code].message
            })
        if (err.code != 500)
            res.status(err.code).json(err);
        else
            res.status(500).json(new CustomError(500, "ERR_SERVER", err.message, err.stack))
    }
}

module.exports = routeErrorHandler