const dateLogger = require("../helpers/dateLoggerHelpers")
const { budgetLimitChecker, amountLimitChecker } = require("../helpers/limitCheckerHelpers")
const emailSender = require("../middleware/emailMiddleware")
const db = require("../models")

async function todayExpenseTrigger() {
    const localDate = new Date().toLocaleString('default', { day: '2-digit' })
    const todayDate = new Date(new Date(new Date().setUTCDate(localDate)).setUTCHours(0, 0, 0, 0))
    const result = await db.expenses.findAll({
        raw: true,
        where: { status: "pending", dueDate: todayDate }
    })
    result.forEach(async (expense) => {
        console.log(`Auto Expense:\t${expense.id}`)

        const userData = await db.users.findOne({
            raw: true,
            where: { id: expense.userId }
        })

        const balance = await db.wallets.findOne({
            raw: true,
            where: { userId: expense.userId }
        })

        if (balance.amount < expense.cost) {
            await db.subscriptions.update(
                { status: 0 },
                { where: { id: expense.subscriptionId } }
            )
            await db.expenses.update(
                { status: "unpaid" },
                { where: { id: expense.id } }
            )
            console.log("This user can't pay this subscriptions")
            // await emailSender("you can't purchase this subscriptions", "haloivan.idn@gmail.com").catch(err => {
            //     console.log(err);
            // })
            return
        }

        const subscriptions = await db.subscriptions.findOne({
            raw: true,
            where: { id: expense.subscriptionId }
        })

        if (subscriptions.typeExpense == "periodic") {
            const nextDueDate = new Date(new Date(subscriptions.dueDate).getTime() + (30 * 24 * 60 * 60 * 1000))
            console.log(`Auto Expense:\tPeriodic detect, try create next expense at ${dateLogger(nextDueDate)}`)

            //** check expense from due date month */
            await budgetLimitChecker(nextDueDate, subscriptions.cost, expense.userId)
                .catch(err => {
                    console.log(`Auto Expense:\tReaching budget limit at next due date`)
                    return
                })

            await amountLimitChecker(subscriptions.cost, expense.userId)
                .catch(err => {
                    console.log(`Auto Expense:\tAmmount of balance can't handle the next expense try to notice this user`)
                })

            const nextExpense = {
                userId: subscriptions.userId,
                subscriptionId: subscriptions.id,
                dueDate: nextDueDate,
                cost: subscriptions.cost
            }

            await db.expenses.create(nextExpense)
                .catch(err => {
                    throw new CustomError(400, "ERR_ADD_EXPENSES", err.errors[0].message)
                })
            console.log(`Auto Expense:\tNext expense at ${dateLogger(nextDueDate)} has been successfully created`)

            await db.subscriptions.update(
                { dueDate: nextDueDate },
                {
                    where: { id: subscriptions.id }
                }).catch(err => {
                    console.log(err)
                })
            console.log(`Auto Expense:\tNext due date ${dateLogger(nextDueDate)} has been set to subscription detail`)
        }

        await db.wallets.update(
            { amount: balance.amount - expense.cost },
            { where: { userId: expense.userId } }
        )
        console.log(`Auto Expense:\tWallets balance has been updated`)
        await db.expenses.update(
            { status: "paid" },
            { where: { id: expense.id } }
        )
        console.log(`Auto Expense:\tExpense with id ${expense.id} successfully paid`)
    })
}

module.exports = todayExpenseTrigger