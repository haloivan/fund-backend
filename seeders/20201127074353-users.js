'use strict';
const faker = require('faker');
const { v4 } = require('uuid');

const users = [
  {
    id: v4(),
    fullName: "demo user",
    email: "demouser@demoemail.com",
    password: "$2b$10$n44LZ32nI2Up3pZWx.E.k.1Y1KExx7u.CohjOrQjvIAR.iWuRKxJC",
    photoProfile: faker.image.avatar(),
    createdAt: new Date(),
    updatedAt: new Date()
  }
]
let count = 0;
while (count < 20) {
  users.push({
    id: v4(),
    fullName: faker.name.findName(),
    email: faker.internet.email(),
    password: "$2b$10$n44LZ32nI2Up3pZWx.E.k.1Y1KExx7u.CohjOrQjvIAR.iWuRKxJC",
    photoProfile: faker.image.avatar(),
    createdAt: new Date(),
    updatedAt: new Date()
  })
  count++
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('users', users)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', {})
  }
};
