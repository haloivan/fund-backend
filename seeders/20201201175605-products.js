'use strict';

const products = [
  {
    id: "b614a581-b1c7-461a-9f3b-259e709d5ffe",
    name: 'Disney+',
    description: "Disney+ Hotstar is the dedicated streaming home for the biggest global and Indonesian hits, all in one place. It brings together your favourite shows, Hollywood movies and award-winning content from Disney, Marvel, Star Wars, Pixar, National Geographic, and many more, as well as exclusive Indonesian premieres, and your favourite Indonesian movies from leading Indonesian studios. Available on select internet- connected screens, Disney + Hotstar offers advertisement - free streaming with a variety of original feature - length films, live - action and animated series, short - form content and documentaries, including series like The Mandalorian and films like Avengers: Endgame, Aladdin and Frozen 2, alongside a huge library of Indonesian cinema releases and homegrown hits. ",
    image: 'https://fund.haloivan.com/files/products/disney-hotstar.png',
    price: 39000,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: "7adf4d75-1f15-4315-b3a9-7b038f22e53c",
    name: 'HBO Max',
    description: "HBO Max is the streaming platform that bundles all of HBO together with even more of your favorite TV series, blockbuster movies, plus new Max Originals. Get comfy, because you’ve got 100 years of epic entertainment in your hands. You’ll have unlimited access to all of HBO, together with even more favorites from Sesame Workshop, DC, Warner Bros., and more.",
    image: 'https://fund.haloivan.com/files/products/hbo-max.png',
    price: 215000,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: "264284f3-faf9-4743-85e8-ea3130e74601",
    name: 'Spotify Premium',
    description: "Spotify makes it easy for you to find the right music or podcast for any time on your phone, your computer, your tablet, and more. There are millions of songs and episodes on Spotify.So whether you're driving, working out, partying, or relaxing, the right music or podcast is always in your hands. Choose what you want to hear or let Spotify surprise you. You can also browse collections belonging to friends, artists, and celebrities or create a radio station and sit back and relax.",
    image: 'https://fund.haloivan.com/files/products/spotify-logo.png',
    price: 50000,
    createdAt: new Date(),
    updatedAt: new Date()
  },
  {
    id: "f6605018-d804-498b-a41a-549b834368f8",
    name: 'Netflix',
    description: "At Netflix, we want to entertain the world. Whatever your taste, and no matter where you live, we give you access to best-in-class TV shows, movies and documentaries. Our members control what they want to watch, when they want it, with no ads, in one simple subscription. We’re streaming in more than 30 languages and 190 countries, because great stories can come from anywhere and be loved everywhere. We are the world’s biggest fans of entertainment, and we’re always looking to help you find your next favorite story.",
    image: 'https://fund.haloivan.com/files/products/netflix-logo.png',
    price: 120000,
    createdAt: new Date(),
    updatedAt: new Date()
  }
]

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('products', products)
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('products', {})
  }
};
