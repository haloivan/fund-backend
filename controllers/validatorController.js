const Ajv = require('ajv')
const yaml = require('js-yaml');
const fs = require('fs');
const CustomError = require('../helpers/customErrorHelper')

const ajv = new Ajv()

const schema = yaml.safeLoad(fs.readFileSync('./controllers/validatorSchema.yml', 'utf8'))

function validatorController(schema_name, body) {
    const validator = ajv.compile(schema[schema_name])
    const isValid = validator(body)
    if (!isValid) {
        throw new CustomError(400, "ERR_VALIDATION",
            "Wrong body",
            validator.errors.map(err => `${err.dataPath} ${err.message}`).join())
    }
}

module.exports = validatorController
