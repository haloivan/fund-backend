const { decryptPass, encryptPass } = require("../helpers/bcryptHelpers");
const CustomError = require("../helpers/customErrorHelper");
const db = require("../models");
const jwt = require('jsonwebtoken');
const validatorController = require("./validatorController");

const secret = process.env.JWT_SECRET

class UserController {
    constructor(data) {
        this.data = data
    }
    async login() {
        validatorController('login', this.data)
        const user = await db.users.findAll({
            raw: true,
            where: { email: this.data.email }
        }).catch(err => {
            throw new CustomError(500, err.code, err.message)
        })
        if (!user.length)
            throw new CustomError(404, "USER_NOT_FOUND", "This email never registered before")
        const isPassMatch = await decryptPass(this.data.password, user[0].password)
        if (!isPassMatch)
            throw new CustomError(400, "ERR_PASS_NOT_MATCH", "Password not match")
        this.data = user[0]
        this.data.token = jwt.sign(this.data, secret, {
            expiresIn: '6h'
        })
        delete this.data.password
    }
    async register() {
        validatorController('register', this.data)
        this.data.password = await encryptPass(this.data.password)
        await db.users.create(this.data)
            .then((result) => {
                this.data = result.get({ plain: true })
            })
            .catch(err => {
                throw new CustomError(400, "ERR_REGISTER_ACCOUNT", err.errors[0].message)
            })
        this.data.token = jwt.sign(this.data, secret, {
            expiresIn: '6h'
        })
        delete this.data.password
    }
    async getUser() {
        const user = await db.users.findAll({
            raw: true,
            where: { id: this.data.id }
        }).catch(err => {
            throw new CustomError(500, err.code, err.message)
        })
        if (!user.length)
            throw new CustomError(404, "USER_NOT_FOUND", "This email never registered before")
        this.data = user[0]
        delete this.data.password
    }
    async updateUser() {
        validatorController('updateUser', this.data)
        if (this.data.password)
            this.data.password = await encryptPass(this.data.password)
        const result = await db.users.update(this.data, { where: { id: this.data.id } })
            .catch(err => {
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "USER_NOT_FOUND", "Wrong users id")
    }
    async deleteUser() {
        validatorController('deleteUser', this.data)
        const result = await db.users.destroy({ where: { id: this.data.id } })
            .catch(err => {
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "USER_NOT_FOUND", "Wrong users id")
    }

}

module.exports = UserController