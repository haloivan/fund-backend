const budgetLimitChecker = require("../helpers/budgetLimitCheckerHelpers");
const CustomError = require("../helpers/customErrorHelper");
const db = require("../models");
const firebaseAdmin = require("../routes/public/notification/firebaseAdmin");
const SubscriptionsController = require("./subscriptionsController");
const validatorController = require("./validatorController");

class NotificationController extends SubscriptionsController { 
    constructor(data) { 
        this.data = data
    }

    async getNotificationNearDueDate() {
        const localDate = new Date().toLocaleString().split('/')[1]
        const todayDate = new Date(new Date(new Date().setUTCDate(localDate)).setUTCHours(0, 0, 0, 0))
        const result = await db.expenses.findAll({
            raw: true,
            where: { status: "pending" }
        }).catch(err => {
            throw new CustomError(500, "ERROR_GET_EXPENSES_DATA", err)
        })
        this.data = result
        // console.log(result);

        // check jika dueDate = today && status pending, send message, paid soon
        if (result.dueDate == todayDate && result.status == "pending") { 
            console.log(`Your subscription ${result.id} is nearly over due`)
        }
        // check jika dueDate > today && status pending, send unsubscribe
        if (result.dueDate > todayDate && result.status == "pending") { 
            this.unsubscribe();
        }
    }

    async getNotificationOverBudget() { 
        const isOverBudget = budgetLimitChecker()
        // const subscriptions = await db.subscriptions.findAll({
        //     raw: true,
        //     where: { id: expense.subscriptionId }
        // })
        // check jikaOverBudget
        if (isOverBudget) { 
            console.log("Your expenses nearly over budget, please setting new budget");
        }
    }
}

module.exports = NotificationController