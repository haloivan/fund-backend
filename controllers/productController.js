// const { decryptPass, encryptPass } = require("../helpers/bcryptHelpers");
const { QueryTypes } = require("sequelize");
const CustomError = require("../helpers/customErrorHelper");
const db = require("../models");
// const jwt = require('jsonwebtoken');
const validatorController = require("./validatorController");

const secret = process.env.JWT_SECRET

class ProductController {
    constructor(data) {
        this.data = data
    }
    async getProduct() {
        let result;
        if (this.data.id)
            result = await db.products.findOne({
                raw: true,
                where: { id: this.data.id }
            })
        else
            result = await db.products.findAll({
                raw: true
            })
        if (!result)
            throw new CustomError(404, "PRODUCT_NOT_FOUND", "Sorry the product not found")
        this.data = result
    }

    async getProductByPopular() { 
        let products = await db.sequelize.query(
            `SELECT COUNT(productId) as counter, productId, name, description, image, price
            FROM  subscriptions s2 
            LEFT JOIN products p2 ON s2.productId = p2.id
            GROUP BY productId
            ORDER by counter DESC 
            LIMIT 2;`,
            {
                raw: true,
                type: QueryTypes.SELECT
            },
            
        )

        if (!products)
            throw new CustomError(404, "PRODUCT_NOT_FOUND", "Sorry the product not found")
        this.data = products
    }
    
    async updateProduct() {
        validatorController('editProduct', this.data)
        const result = await db.products.update(this.data, { where: { id: this.data.id } })
            .catch(err => {
                console.log(err);
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "USER_NOT_FOUND", "Wrong products id")
    }
    async deleteProduct() {
        validatorController('deleteProduct', this.data)
        const result = await db.products.destroy({ where: { id: this.data.id } })
            .catch(err => {
                console.log(err);
                // throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        return result
        // if (result == 0)
        //     throw new CustomError(404, "USER_NOT_FOUND", "Wrong product id")
    }

}

module.exports = ProductController