const CustomError = require("../helpers/customErrorHelper");
const db = require("../models");
const validatorController = require("./validatorController");

class BankAccountController {
    constructor(data) {
        this.data = data
    }

    async getBankAccount() {
        if (!this.data.userId) {
            throw new CustomError(400, "USER_ID_NEEDED", "user id needed in query")
        }
        const result = await db.banks.findAll({
            raw: true,
            where: { userId: this.data.userId }
        }).catch(err => {
            throw new CustomError(500, err.code, err.message)
        })
        if (!result.length)
            throw new CustomError(404, "DATA_NOT_FOUND", `${this.data.userId} haven't add the card yet`)
        this.data = result
    }

    async addBankAccount() {
        validatorController('addBankAccount', this.data)
        await db.banks.create(this.data)
            .then((result) => {
                this.data = result.get({ plain: true })
            })
            .catch(err => {
                if (err.errors[0].message == "banks.cardNumber must be unique")
                    throw new CustomError(400, "ERR_ADD_BANK_ACCOUNT", "card number has been added before")
                else
                    throw new CustomError(400, "ERR_ADD_BANK_ACCOUNT", err)
            })
    }

    async deleteBankAccount() {
        validatorController('deleteBankAccount', this.data)
        const result = await db.banks.destroy({ where: { id: this.data.id } })
            .catch(err => {
                // console.log(err);
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        // return result
        if (result == 0)
            throw new CustomError(404, "BANK_NOT_FOUND", "Wrong card id")
    }

}

module.exports = BankAccountController