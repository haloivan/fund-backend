// const { decryptPass, encryptPass } = require("../helpers/bcryptHelpers");
const CustomError = require("../helpers/customErrorHelper");
const db = require("../models");
const validatorController = require("./validatorController");

class BudgetController {
    constructor(data) {
        this.data = data
    }

    async getBudget() {
        {
            if (!this.data.userId)
                throw new CustomError(400, "QUERY_USER_ID", "User Id needed in query")

            const budget = await db.budgets.findAll({
                raw: true,
                where: { userId: this.data.userId }
            }).catch(err => {
                throw new CustomError(500, err.code, err.message)
            })
            // console.log(budget);
            if (!budget.length)
                throw new CustomError(404, "DATA_NOT_FOUND", "You havent add the budget yet")
            this.data = budget[0]
        }
    }

    async addBudget() {
        validatorController('addBudget', this.data)
        const isUserExist = await db.budgets.findAll({
            raw: true,
            where: { userId: this.data.userId }
        })
        if (isUserExist.length)
            throw new CustomError(400, "USER_EXIST", "User have saved budget")

        //** Creating User budget */
        await db.budgets.create(this.data)
            .then((result) => {
                this.data = result.get({ plain: true })
            })
            .catch(err => {
                throw new CustomError(400, "ERR_INPUT_BUDGET", err.errors[0].message)
            })
    }
    async editBudget() {
        validatorController('editBudget', this.data)
        const result = await db.budgets.update(this.data, { where: { id: this.data.id } })
            .catch(err => {
                throw new CustomError(400, "ERR_EDIT_BUDGET", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "ERR_EDIT_BUDGET", "Wrong budget id")
    }
    async deleteBudget() {
        validatorController('deleteBudget', this.data)
        const result = await db.budgets.destroy({ where: { id: this.data.id } })
            .catch(err => {
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "USER_NOT_FOUND", "Wrong product id")
    }

}

module.exports = BudgetController