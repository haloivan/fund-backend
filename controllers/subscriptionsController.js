const { QueryTypes } = require("sequelize");
const CustomError = require("../helpers/customErrorHelper");
const { budgetLimitChecker, amountLimitChecker } = require("../helpers/limitCheckerHelpers");
const setDueDate = require("../helpers/setDueDateHelper");
const db = require("../models");
const validatorController = require("./validatorController");

class SubscriptionsController {
    constructor(data) {
        this.data = data
    }
    async addSubscriptions() {
        validatorController('addSubscription', this.data)

        //**cheking user data */
        const user = await db.users.findAll({
            raw: true,
            where: { id: this.data.userId }
        }).catch(err => {
            throw new CustomError(500, err.code, err.message)
        })
        if (!user.length)
            throw new CustomError(404, "USER_NOT_FOUND", "userId not found")

        //**cheking product data */
        const product = await db.products.findAll({
            raw: true,
            where: { id: this.data.productId }
        }).catch(err => {
            throw new CustomError(500, err.code, err.message)
        })
        if (!product.length)
            throw new CustomError(404, "PRODUCT_NOT_FOUND", "productId not found")
        this.data.cost = product[0].price

        //**cheking user subscriptions data */
        const isUserSubscribe = await db.subscriptions.findAll({
            raw: true,
            where: {
                userId: this.data.userId,
                productId: this.data.productId,
                status: true
            }
        }).catch(err => {
            throw new CustomError(500, err.code, err.message)
        })
        if (isUserSubscribe.length)
            throw new CustomError(400, "USER_SUBSCRIPTIONS_ACTIVE", "This user have active subscriptions")
        if (!['onetime', 'periodic'].includes(this.data.typeExpense))
            throw new CustomError(400, "WRONG_TYPE_EXPENSE", "Choose onetime or periodic for type of expense")

        const getToday = new Date().toLocaleString('default', { day: '2-digit' })

        //** set start subscription and 1 month subscriptions */
        this.data.startDate = new Date(new Date(new Date().setUTCHours(0, 0, 0, 0)).setUTCDate(getToday))
        this.data.endDate = new Date(new Date(this.data.startDate).getTime() + (30 * 24 * 60 * 60 * 1000))

        //** set due date */
        let dueDate;
        if (!this.data.dueDate) {
            dueDate = this.data.startDate
        } else {
            dueDate = setDueDate(this.data.dueDate)
        }
        this.data.dueDate = dueDate

        //** check expense from due date month */
        await budgetLimitChecker(dueDate, this.data.cost, this.data.userId)

        //?? check amount limit */
        await amountLimitChecker(this.data.cost, this.data.userId)

        // //** save to db */
        await db.subscriptions.create(this.data)
            .then((result) => {
                this.data = result.get({ plain: true })
            })
            .catch(err => {
                throw new CustomError(400, "ERR_ADD_SUBSCRIPTIONS", err.errors[0].message)
            })

        const expense = {
            userId: this.data.userId,
            subscriptionId: this.data.id,
            dueDate,
            cost: this.data.cost
        }
        await db.expenses.create(expense)
            .catch(err => {
                console.log(err);
                throw new CustomError(400, "ERR_ADD_EXPENSES", err.errors[0].message)
            })
    }
    async userActiveSubscribe() {
        if (!this.data.userId)
            throw new CustomError(400, "QUERY_USER_ID", "User Id needed in query")
        const result = await db.subscriptions.findAll({
            raw: true,
            where: { userId: this.data.userId, status: true }
        })
        for (let i = 0; i < result.length; i++) {
            const productDetail = await db.products.findOne({
                raw: true,
                where: { id: result[i].productId }
            })
            result[i].productName = productDetail.name
            result[i].productImage = productDetail.image
        }
        this.data = result
    }
    async unsubscribe() {
        validatorController('unsubscribe', this.data)
        const result = await db.subscriptions.update({ status: false }, { where: { id: this.data.id } })
            .catch(err => {
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "WRONG_SUBS_ID", "This subscriptions id not found")
    }
}

module.exports = SubscriptionsController