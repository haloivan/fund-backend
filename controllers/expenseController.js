const CustomError = require("../helpers/customErrorHelper")
const setDueDate = require("../helpers/setDueDateHelper")
const db = require("../models");
const { QueryTypes } = require("sequelize");
const { budgetLimitChecker, amountLimitChecker } = require("../helpers/limitCheckerHelpers");

class ExpenseController {
    constructor(data) {
        this.data = data
    }
    async getHistory() {
        if (!this.data.userId)
            throw new CustomError(400, "USER_ID_REQUIRED", "Insert user id in param")
        const result = await db.sequelize.query(
            `SELECT p.name, s.typeExpense, e.dueDate, e.cost FROM expenses as e
            JOIN subscriptions as s ON e.subscriptionId = s.id
            JOIN products as p ON s.productId = p.id
            WHERE MONTH(e.dueDate) = ${new Date().getUTCMonth() + 1}
            AND e.status = "paid"
            AND e.userId = "${this.data.userId}"`,
            { raw: true, type: QueryTypes.SELECT }
        )
        let totalExpense = 0;
        result.forEach(element => {
            totalExpense = totalExpense + element.cost
        });
        this.data = [
            { totalExpense },
            { listExpense: result }
        ]
    }
    async getUpcoming() {
        if (!this.data.userId)
            throw new CustomError(400, "USER_ID_REQUIRED", "Insert user id in param")
        const result = await db.sequelize.query(
            `SELECT p.name, e.dueDate FROM expenses as e
            JOIN subscriptions as s ON e.subscriptionId = s.id
            JOIN products as p ON s.productId = p.id
            WHERE e.status = "pending"
            AND e.userId = "${this.data.userId}"`,
            { raw: true, type: QueryTypes.SELECT }
        )
        this.data = result
    }
    async getTotalCostUpcoming() {
        if (!this.data.userId)
            throw new CustomError(400, "USER_ID_REQUIRED", "Insert user id in param")
        const result = await db.expenses.findAll({
            raw: true,
            where: { userId: this.data.userId, status: "pending" }
        })
        this.data = 0
        if (result.length)
            result.forEach(i => {
                this.data = this.data + i.cost
            });
    }
    async getExpenseGraph() {
        if (!this.data.userId)
            throw new CustomError(400, "USER_ID_REQUIRED", "Insert user id in param")
        const result = await db.sequelize.query(
            `SELECT dueDate, cost FROM expenses
            WHERE dueDate < NOW()
            AND dueDate > DATE_ADD(NOW(), INTERVAL - 4 MONTH)
            AND status = "paid"
            AND userId = "${this.data.userId}"`,
            { raw: true, type: QueryTypes.SELECT }
        )

        function getMonthYearString(date) {
            const month = new Date(date).toLocaleString('default', { month: 'long' })
            const year = new Date(date).toLocaleString('default', { year: 'numeric' })
            return `${month} ${year}`
        }

        const listExpense = {}
        while (Object.keys(listExpense).length < 4) {
            const num = new Date().getMonth() - Object.keys(listExpense).length
            const monthYear = getMonthYearString(new Date().setMonth(num))
            listExpense[monthYear] = 0
        }
        result.forEach(i => {
            const monthYear = getMonthYearString(i.dueDate)
            listExpense[monthYear] = listExpense[monthYear] + i.cost
        });
        this.data = []
        Object.keys(listExpense).forEach(ele => {
            this.data.push({
                month: ele,
                totals: listExpense[ele]
            })
        })
    }
}

module.exports = ExpenseController