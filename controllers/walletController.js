const CustomError = require("../helpers/customErrorHelper");
const db = require("../models");
const validatorController = require("./validatorController");

class WalletController {
    constructor(data) {
        this.data = data
    }

    async getWallet() {
        {
            if (!this.data.userId)
                throw new CustomError(400, "USER_ID_NEEDED", "User id needed in the  query")
            const wallet = await db.wallets.findAll({
                // raw: true,
                where: { userId: this.data.userId }
            }).catch(err => {
                throw new CustomError(500, err.code, err.message)
            })
            if (!wallet.length)
                throw new CustomError(404, "DATA_NOT_FOUND", "You havent add the wallet yet")
            this.data = wallet[0]
        }
    }

    async addWallet() {
        validatorController('addWallet', this.data)
        const isWalletExist = await db.wallets.findAll({
            where: {
                userId: this.data.userId,
                // amount: this.data.amount
            }
        })
        if (isWalletExist && isWalletExist.length) { 
            throw new CustomError(400, "ERR_INPUT_WALLET", "You already create wallet, please check your wallet")
        }
        // console.log('addWallet');
        await db.wallets.create(this.data)
            .then((result) => {
                this.data = result.get({ plain: true })
            })
            .catch(err => {
                throw new CustomError(400, "ERR_INPUT_BUDGET", err.errors[0].message)
            })
        
    }
    async topupWallet() {
        validatorController('topupWallet', this.data)
        const wallet = await db.wallets.findAll({
            raw: true,
            where: {
                id: this.data.id
            }
        })
        this.data.amount = this.data.amount + wallet[0].amount
        const result = await db.wallets.update(this.data, {
            where: {
                id: this.data.id
            }
        })
            .catch(err => {
                console.log(err);
                throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        if (result == 0)
            throw new CustomError(404, "USER_NOT_FOUND", "Wrong budget id")
    }
    async deleteWallet() {
        validatorController('deleteWallet', this.data)
        const result = await db.wallets.destroy({ where: { id: this.data.id } })
            .catch(err => {
                console.log(err);
                // throw new CustomError(400, "ERR_ACCOUNT", err.errors[0].message)
            })
        return result
        // if (result == 0)
        //     throw new CustomError(404, "USER_NOT_FOUND", "Wrong product id")
    }

}

module.exports = WalletController