require('dotenv').config()
const { random } = require("faker");
const getPort = require('get-port');
const execute = require('./executeUtil');

class ServerHelper { 
    constructor(routePath) { 
        this.routePath = routePath
        this.dbName = random.alphaNumeric(10)
    }
    async start() { 
        process.env.DB_DATABASE = this.dbName
        this.db = require("../models");
        await execute('sequelize db:create')
        await execute('sequelize db:migrate')

        const express = require('express')
        const app = express()
        app.use(express.json())
        const publicRoute = require('../routes/public/' + this.routePath)
        // const privateRoute = require('../routes/private/' + this.routePath)
        app.use(publicRoute)
        // app.use(privateRoute)
        this.port = await getPort()
        this.app = app.listen(this.port)  
    }
    async drop() { 
        process.env.DB_DATABASE = this.dbName
        await execute('sequelize db:drop')
        this.app.close()
    }
}

module.exports = ServerHelper