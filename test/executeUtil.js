const exec = require('child_process').exec // built-in untuk menjalan perintah seolah olah didalam terminal
function execute(command) {
    return new Promise((resolve, reject) => { 
        exec(command, (error, stdout, stderr) => { 
            if (error)
                reject({ error, stderr })
            else
                resolve(stdout)
        })
    })
}

module.exports = execute