const chai = require('chai')
const chaiHttp = require('chai-http')
const { name, internet } = require('faker')
const ServerHelper = require('../../../serverHelper')
const server = new ServerHelper('auth/registerRoute')

chai.use(chaiHttp)
const { expect, request } = chai

before(async function () { 
    await server.start()
    this.db = server.db
    this.app = server.app
    this.port = server.port
    // this.appListener = server.appListener
})
after(async function () { 
    await server.drop()
})

describe('register route', () => {

    it('should can register', async function () { 
        const userBody = {
            fullName: name.findName(),
            email: internet.email(),
            password: internet.password(12),
        }

        const response = await request(this.app)
            .post('/register')
            .send(userBody)
        expect(response.status).is.equal(200)

        const user = await this.db.users.findOne({
            raw: true,
            where: { id: response.body.id}
        })
        expect(user).is.not.null

    })
    it("should can't register with wrong body", async function () { 
        const userBody = {
            email: internet.email(),
            password: internet.password(12),
        }

        const response = await request(this.app)
            .post('/register')
            .send(userBody)
        expect(response.status).is.equal(400)
    })
    it("should can't register available user", async function () { 
        const userBody = {
            fullName: name.findName(),
            email: internet.email(),
            password: internet.password(12),
        }
        await this.db.users.create(userBody)

        const response = await request(this.app)
            .post('/register')
            .send(userBody)
        
        // console.log(response);
        expect(response.status).is.equal(400)
    })

})