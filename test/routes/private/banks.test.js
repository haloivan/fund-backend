const { name, internet, image, random } = require("faker")
const { v4 } = require("uuid")
const DatabaseHelper = require("../../databaseHelper")
const database = new DatabaseHelper

const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai

let db 
before( async () => { 
   db = await database.start()
})

after( async () => { 
    await database.drop()
})

const userBody = {
    fullName: name.findName(),
    // email: internet.email(),
    password: internet.password()
}
const bankBody = {
    // id: v4(),
    cardNumber: random.number(),
    type: random.alpha()
}

beforeEach(async function () { 
    this.newUser = await db.users.create({
        // id: userBody.id,
        email: internet.email(),
        ...userBody
    })
})

describe("Model bank account",  () => {
    it('should can add bank account ', async function() { 
        // const newUser = await db.users.create(userBody)
        const bank = await db.banks.create({
            userId: this.newUser.id,
            ...bankBody
        })
        // console.log(bank);
        expect(bank.dataValues).to.have.property('id')
    })
    it('should cant add bank account if no cardNumber', async function() { 
        const clonedBankBody = { ...bankBody }
        delete clonedBankBody.cardNumber
        await expect(db.banks.create(clonedBankBody)).to.eventually.be.rejected
    })
    it('should cant add bank account if no type', async function() { 
        const clonedBankBody = { ...bankBody }
        delete clonedBankBody.type
        await expect(db.banks.create(clonedBankBody)).to.eventually.be.rejected
    })
    it('should cant add bank account if no userId is not match', async function() { 
        await expect(db.banks.create(bankBody)).to.eventually.be.rejected
    })
    it('should can get bank account', async function() {
        // const newBanks = await db.banks.create({
        //     userId: this.newUser.id,
        //     ...bankBody
        // })
        // this.newBanks.id = newBanks.id
        // // console.log(bank);
        const getBank = await db.banks.findOne({
            raw: true,
            where: {
                cardNumber: bankBody.cardNumber
            }
        })
        // console.log(getBank);
        expect(getBank).is.not.null
    })
    it('should can edit bank account', async () => { 
        const newCardNumber = random.number()
        const newType = random.alpha()
        // const newBank = await db.banks.create({
        //     userId: this.newUser.id,
        //     ...bankBody
        // })
        const editedBank = await db.banks.update({
            cardNumber: newCardNumber,
            type: newType
        },
            {
                where: {
                    cardNumber: bankBody.cardNumber
                }
            }
        )
        expect(editedBank[0]).is.equal(1)
    })
    it('should can delete bank account', async function() { 
        const newBank = await db.banks.create({
            userId: this.newUser.id,
            ...bankBody
        })
        // console.log(newBank);
        const deletedBank = await db.banks.destroy({
            where: {
                id: newBank.id
            }
        })
        // console.log(deletedBank);
        expect(deletedBank).is.equal(1)
    })
    
})