const { name, internet, image, random } = require("faker")
const { v4 } = require("uuid")
const DatabaseHelper = require("../../databaseHelper")
const database = new DatabaseHelper

const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai

let db 
before( async () => { 
   db = await database.start()
})

after( async () => { 
    await database.drop()
})

const userBody = {
    fullName: name.findName(),
    password: internet.password()
}
const budgetBody = {
    amount: random.number()
}

beforeEach(async function () { 
    this.newUser = await db.users.create({
        email: internet.email(),
        ...userBody
    })
})

describe("Model budget",  () => {
    it('should can add budget ', async function() { 
        // const newUser = await db.users.create(userBody)
        const budget = await db.budgets.create({
            userId: this.newUser.id,
            ...budgetBody
        })
        // console.log(budget);
        expect(budget.dataValues).to.have.property('id')
    })
    it('should cant add budget if no userId is not match', async function() { 
        await expect(db.budgets.create(budgetBody)).to.eventually.be.rejected
    })
    it('should can get budget', async function() {
        const newBudget = await db.budgets.create({
            userId: this.newUser.id,
            ...budgetBody
        })

        const getBudget = await db.budgets.findOne({
            raw: true,
            where: {
                id: newBudget.id
            }
        })
        expect(getBudget).is.not.null
    })
    // it('should can edit budget', async () => { 
    //     const newAmount = random.number()
    //     const newBudgets = await db.budgets.create({
    //         userId: this.newUser.id,
    //         ...budgetBody
    //     })
    //     const editedBudget = await db.users.update({
    //         amount: newAmount
    //     },
    //         {
    //             where: {
    //                 id: newBudgets.id
    //             }
    //         }
    //     )
    //     expect(editedBudget[0]).is.equal(1)
    // })
    it('should can delete budget', async function() { 
        const newBudget = await db.budgets.create({
            userId: this.newUser.id,
            ...budgetBody
        })
        // console.log(newBank);
        const deletedBank = await db.budgets.destroy({
            where: {
                id: newBudget.id
            }
        })
        // console.log(deletedBank);
        expect(deletedBank).is.equal(1)
    })
    
})