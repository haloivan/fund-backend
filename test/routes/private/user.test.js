const { name, internet } = require("faker")
const DatabaseHelper = require("../../databaseHelper")
const database = new DatabaseHelper()

const chaiPromise = require('chai-as-promised')
const chai = require("chai")
const { v4 } = require("uuid")
chai.use(chaiPromise)
const { expect } = chai

let db 
before( async () => { 
   db = await database.start()
})
after( async () => { 
    await database.drop()
})

const userBody = {
    fullName: name.findName(),
    password: internet.password()
}

beforeEach(async function () { 
    this.newUser = await db.users.create({
        email: internet.email(),
        ...userBody
    })
})

describe("Model user",  () => {
    it('should can add user ', async function() { 
        expect(this.newUser.dataValues).to.have.property('id')
    })
    it('should cant add user if no fullName', async () => { 
        const cloneUserBody = { ...userBody }
        delete cloneUserBody.fullName
        await expect(db.users.create(cloneUserBody)).to.eventually.be.rejected
    })
    it('should cant add user if no email', async () => { 
        const cloneUserBody = { ...userBody }
        delete cloneUserBody.email
        await expect(db.users.create(cloneUserBody)).to.eventually.be.rejected
    })
    it('should cant add user if no password', async () => { 
        const cloneUserBody = { ...userBody }
        delete cloneUserBody.password
        await expect(db.users.create(cloneUserBody)).to.eventually.be.rejected
    })
    it('should can get user', async function() {
        const getUser = await db.users.findOne({
            where: { id: this.newUser.id}
        })
        expect(getUser).is.not.null
    })
    it('should can edit user', async function() { 
        const newFullName = name.findName()
        const newEmail = internet.email()
        const newPassword = internet.password()
        const user = await db.users.update({
            fullName: newFullName,
            email: newEmail,
            password: newPassword
        },
            {
                where: {
                    id: this.newUser.id
                }
            }
        )
        // console.log(user);
        expect(user[0]).is.equal(1)
    })
    it('should can delete user', async function() { 
        const user = await db.users.destroy({
            where: {
                id: this.newUser.id
            }
        })
        // console.log(user);
        expect(user).is.equal(1)
    })
    
})