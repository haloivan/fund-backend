const { name, internet, image, random } = require("faker")
const { v4 } = require("uuid")
const DatabaseHelper = require("../../databaseHelper")
const database = new DatabaseHelper

const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai

let db 
before( async () => { 
   db = await database.start()
})

after( async () => { 
    await database.drop()
})

const userBody = {
    fullName: name.findName(),
    password: internet.password()
}
const walletBody = {
    amount: random.number()
}

beforeEach(async function () { 
    this.newUser = await db.users.create({
        email: internet.email(),
        ...userBody
    })
})

describe("Model wallet",  () => {
    it('should can top up wallet ', async function() { 
        // const newUser = await db.users.create(userBody)
        const wallet = await db.wallets.create({
            userId: this.newUser.id,
            ...walletBody
        })
        // console.log(wallet);
        expect(wallet.dataValues).to.have.property('id')
    })
    it('should cant add wallet if no userId is not match', async function() { 
        await expect(db.wallets.create(walletBody)).to.eventually.be.rejected
    })
    it('should can get wallet', async function() {
        const newWallet = await db.wallets.create({
            userId: this.newUser.id,
            ...walletBody
        })

        const getwallet = await db.wallets.findOne({
            raw: true,
            where: {
                id: newWallet.id
            }
        })
        expect(getwallet).is.not.null
    })
    it('should can delete wallet', async function() { 
        const newwallet = await db.wallets.create({
            userId: this.newUser.id,
            ...walletBody
        })
        // console.log(newBank);
        const deletedBank = await db.wallets.destroy({
            where: {
                id: newwallet.id
            }
        })
        // console.log(deletedBank);
        expect(deletedBank).is.equal(1)
    })
    
})