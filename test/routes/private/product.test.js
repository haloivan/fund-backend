const { name, internet, random, lorem } = require("faker")
const DatabaseHelper = require("../../databaseHelper")
const database = new DatabaseHelper()

const chaiPromise = require('chai-as-promised')
const chai = require("chai")
const { v4 } = require("uuid")
chai.use(chaiPromise)
const { expect } = chai

let db 
before( async () => { 
   db = await database.start()
})
after( async () => { 
    await database.drop()
})

const productBody = {
    name: name.findName(),
    description: lorem.paragraph(2),
    image: random.image(),
    price: random.number()
}

beforeEach(async function () { 
    this.newproduct = await db.products.create({
        email: internet.email(),
        ...productBody
    })
})

describe("Model product",  () => {
    it('should can add product ', async function() { 
        expect(this.newproduct.dataValues).to.have.property('id')
    })
    it("should can't add product if no name", async () => { 
        const clonedProductBody = { ...productBody }
        delete clonedProductBody.name
        await expect(db.products.create(clonedProductBody)).to.eventually.be.rejected
    })
    it("should can't add product if no description", async () => { 
        const clonedProductBody = { ...productBody }
        delete clonedProductBody.description
        await expect(db.products.create(clonedProductBody)).to.eventually.be.rejected
    })
    it("should can't add product if no price", async () => { 
        const clonedProductBody = { ...productBody }
        delete clonedProductBody.price
        await expect(db.products.create(clonedProductBody)).to.eventually.be.rejected
    })
    it('should can get product', async function() {
        const getproduct = await db.products.findOne({
            where: { id: this.newproduct.id}
        })
        expect(getproduct).is.not.null
    })
    it('should can edit product', async function() { 
        const newName =  name.findName()
        const newDescription = lorem.paragraph(2)
        const newImage = random.image()
        const newPrice =  random.number()
        const product = await db.products.update({
            name: newName,
            description: newDescription,
            image: newImage,
            price: newPrice
        },
            {
                where: {
                    id: this.newproduct.id
                }
            }
        )
        // console.log(product);
        expect(product[0]).is.equal(1)
    })
    it('should can delete product', async function() { 
        const product = await db.products.destroy({
            where: {
                id: this.newproduct.id
            }
        })
        // console.log(product);
        expect(product).is.equal(1)
    })
    
})