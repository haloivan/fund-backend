const { name, internet, image, random, date } = require("faker")
const { v4 } = require("uuid")
const DatabaseHelper = require("../../databaseHelper")
const database = new DatabaseHelper

const chaiPromise = require('chai-as-promised')
const chai = require("chai")
chai.use(chaiPromise)
const { expect } = chai

let db 
before( async () => { 
   db = await database.start()
})

after( async () => { 
    await database.drop()
})

const userBody = {
    fullName: name.findName(),
    password: internet.password()
}
const subscriptionBody = {
    cost: random.number(),
    typeExpense: random.alpha(),
    dueDate: 2021-01-15,
    startDate: 2020-12-19,
    endDate: 2021-01-18,
    // status: 'periodic'
}

beforeEach(async function () { 
    this.newUser = await db.users.create({
        email: internet.email(),
        ...userBody
    })
})

describe("Model subscription", () => {
    
    /**
     * ini masih belum bisa
     */
    // it('should can add subscription ', async function() { 
    //     // const newUser = await db.users.create(userBody)
    //     const subscription = await db.subscriptions.create({
    //         userId: this.newUser.id,
    //         ...subscriptionBody
    //     })
    //     console.log(subscription);
    //     expect(1).is.equal(1)
    // })
    it('should cant add subscription if no userId is not match', async function() { 
        await expect(db.subscriptions.create(subscriptionBody)).to.eventually.be.rejected
    })
    // it('should can get subscription', async function() {
    //     const newsubscription = await db.subscriptions.create({
    //         userId: this.newUser.id,
    //         ...subscriptionBody
    //     })

    //     const getSubscription = await db.subscriptions.findOne({
    //         raw: true,
    //         where: {
    //             id: newsubscription.id
    //         }
    //     })
    //     console.log(getSubscription);
    //     expect(1).is.equal(1)
    //     // expect(getsubscription).is.not.null
    // })
    
})