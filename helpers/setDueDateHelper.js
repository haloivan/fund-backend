function setDueDate(date) {
    const today = new Date()
    let dueDate;
    if (date < today.getUTCDate()) {
        dueDate = new Date(
            new Date(new Date(today.setUTCHours(0, 0, 0, 0)).setUTCDate(date)).setMonth(today.getUTCMonth() + 1)
        )
    } else {
        dueDate = new Date(new Date(today.setUTCHours(0, 0, 0, 0)).setUTCDate(date))
    }
    return dueDate
}

module.exports = setDueDate