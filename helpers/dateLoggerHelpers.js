function dateLogger(date) {
    monthShort = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec'
    ]
    const _ = new Date(date)
    const day = _.getUTCDate()
    const month = monthShort[_.getUTCMonth()]
    const year = _.getUTCFullYear()
    return `${day}-${month}-${year}`
}

module.exports = dateLogger