const db = require("../models");
const { QueryTypes } = require("sequelize");
const CustomError = require("./customErrorHelper");

async function budgetLimitChecker(dueDate, cost, userId) {
    //** check expense from due date month */
    const totalExpense = await db.sequelize.query(
        `SELECT SUM(cost) as value FROM expenses
        WHERE MONTH(dueDate) = ${new Date(dueDate).getUTCMonth() + 1}
        AND userId = "${userId}"`,
        { raw: true, type: QueryTypes.SELECT }
    )
    const budgetLimit = await db.budgets.findAll({
        raw: true,
        where: { userId }
    }).catch(err => {
        throw new CustomError(400, "BUDGET_ERR", err)
    })
    if (!budgetLimit[0].amount)
        throw new CustomError(400, "BUDGET_ERR", "User must have budget limit first")

    if (!totalExpense[0].value == null) {
        if ((cost + parseInt(totalExpense[0].value)) > budgetLimit[0].amount)
            throw new CustomError(400, "BUDGET_LIMIT_REACHED", "User can't purchase this subscription because budget limitation")
    } else {
        if (cost > budgetLimit[0].amount)
            throw new CustomError(400, "BUDGET_LIMIT_REACHED", "This user don't have any expense, but user can't purchase this subscription because budget limitation")
    }
}

async function amountLimitChecker(cost, userId) {
    const totalPendingExpense = await db.sequelize.query(
        `SELECT SUM(cost) as value FROM expenses
        WHERE status = "pending"
        AND userId = "${userId}"`,
        { raw: true, type: QueryTypes.SELECT }
    )

    const walletAmount = await db.wallets.findAll({
        raw: true,
        where: { userId }
    }).catch(err => {
        throw new CustomError(400, "WALLET_ERR", err)
    })
    if (!walletAmount[0].amount)
        throw new CustomError(400, "WALLET_ERR", "User must create wallet first")

    if (!totalPendingExpense[0].value == null) {
        if ((cost + parseInt(totalPendingExpense[0].value)) > walletAmount[0].amount)
            throw new CustomError(400, "BALANCE_LOW", "User can't purchase this subscription because balance can't pay for the next expenses")
    } else {
        if (cost > walletAmount[0].amount)
            throw new CustomError(400, "BALANCE_LOW", "User can't purchase this subscription because balance can't pay for the next expenses")
    }

}

module.exports = {
    budgetLimitChecker,
    amountLimitChecker
}