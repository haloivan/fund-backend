const { QueryTypes } = require("sequelize/types");
const db = require("../models");
const CustomError = require("./customErrorHelper");

async function overDueChecker(dueDate, userId) { 

    // nentuin tanggal
    const dueDates = await db.sequelize.query(
        `SELECT dueDate FROM expenses
        WHERE YEAR(dueDate) = ${new Date(dueDate).getUTCFullYear() + 1}
            and MONTH(dueDate) =  ${new Date(dueDate).getUTCMonth() + 1}
             and DAY(dueDate) = ${new Date(dueDate).getUTCDay + 1}
             and status = "pending"
             and userId = "${userId}"`,
        {raw: true, type: QueryTypes.SELECT}
    )


    // ngambil data dueDate
    const alreadyDueDate = await db.expenses.findAll({
        raw : true,
        where: {
            userId
        }
    }).catch(err => { 
        throw new CustomError(400, "DUE_DATE", err)
    })
    if (!dueDates == null) { 
        // if (parameter) { 

        // }
    }

}

module.exports = {
    overDueChecker
}